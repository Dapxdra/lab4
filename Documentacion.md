---
title: "Documentación Lab4"
author: "Daniel Araya y Ronald Sibaja"
output: PDF
---

4. Conectarse con ODBC llamado Lab4
    Abrimos nuestro configurador ODBC, en la pestaña System DSN, agregamos una nueva, selecionamos la opción "Oracle in OraDB21home1" y damos en finalizar. Luego agregamos un nombre en este caso "Lab4", en el TSN Server name colocamos "XE" y el usuario colocamos "system" Y así se conecta la oracle mediante ODBC.
5. Se crea un archivo .Reviron con las credenciales de la configuración de ODBC y el sqlDeveloper.
6.  odbcConnect y sqlQuery e importe las tablas (cliente, pedido y 
detalle_pedido) para realizar un análisis exploratorio, para lo siguiente se realiza de esta manera:        clientes<-sqlQuery(conOracle,"select * from cliente")
pedidos<-sqlQuery(conOracle,"select * from pedido")
detalle_pedido<-sqlQuery(conOracle,"select * from detalle_pedido")

View(clientes)
View(pedidos)
View(detalle_pedido)